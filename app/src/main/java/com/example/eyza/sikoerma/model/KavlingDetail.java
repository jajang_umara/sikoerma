package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class KavlingDetail {
    private String block,project,site,address;
    @SerializedName("kavling_no") private int kavlingNo;
    @SerializedName("status_legal") private String status;
    @SerializedName("kavling_condition") private String kondisi;
    @SerializedName("included_bonus") private List<KavlingBonus> kavlingBonuses = new ArrayList<>();
    private List<KavlingProgress> progress = new ArrayList<>();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public int getKavlingNo() {
        return kavlingNo;
    }

    public void setKavlingNo(int kavlingNo) {
        this.kavlingNo = kavlingNo;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<KavlingBonus> getKavlingBonuses() {
        return kavlingBonuses;
    }

    public void setKavlingBonuses(List<KavlingBonus> kavlingBonuses) {
        this.kavlingBonuses = kavlingBonuses;
    }

    public List<KavlingProgress> getProgress() {
        return progress;
    }

    public void setProgress(List<KavlingProgress> progress) {
        this.progress = progress;
    }
}

