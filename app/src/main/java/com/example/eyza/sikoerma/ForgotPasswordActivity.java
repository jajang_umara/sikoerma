package com.example.eyza.sikoerma;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.BaseResponse;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ForgotPasswordActivity extends AppCompatActivity {

    private AwesomeValidation awesomeValidation;
    private ProgressDialog pd;
    private CompositeDisposable disposable = new CompositeDisposable();
    private BaseApiServices api;

    @BindViews({R.id.old_password,R.id.new_password})
    List<EditText> texts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");

        ButterKnife.bind(this);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(this,R.id.old_password, RegexTemplate.NOT_EMPTY,R.string.required_field);
        awesomeValidation.addValidation(this,R.id.new_password, RegexTemplate.NOT_EMPTY,R.string.required_field);
        awesomeValidation.addValidation(this,R.id.confirm_password, R.id.new_password,R.string.password_not_match);

        api = RetrofitClient.getClient().create(BaseApiServices.class);
    }

    @OnClick(R.id.btn_login)
    public void changePassword() {


        if (awesomeValidation.validate()){
            pd.show();
            String token = Session.getToken(ForgotPasswordActivity.this);
            Map<String,String> fields = new HashMap<>();
                fields.put("api_username","kurma");
                fields.put("api_password","100%kurma");
                fields.put("login_token",token);
                fields.put("old_password",texts.get(0).getText().toString().trim());
                fields.put("new_password",texts.get(0).getText().toString().trim());

            disposable.add(api.changePassword(fields)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<BaseResponse>(){
                        @Override
                        public void onSuccess(BaseResponse responseLogin) {
                            Toast.makeText(getApplicationContext(),responseLogin.getMsg(),Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    }));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
