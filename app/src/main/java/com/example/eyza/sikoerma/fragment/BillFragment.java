package com.example.eyza.sikoerma.fragment;


import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eyza.sikoerma.DetailBillActivity;
import com.example.eyza.sikoerma.MainActivity;
import com.example.eyza.sikoerma.R;
import com.example.eyza.sikoerma.adapter.BillAdapter;
import com.example.eyza.sikoerma.adapter.KavlingAdapter;
import com.example.eyza.sikoerma.adapter.RecyclerTouchListener;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.Bill;
import com.example.eyza.sikoerma.model.Kavling;
import com.example.eyza.sikoerma.model.ResponseBill;
import com.example.eyza.sikoerma.model.ResponseKavling;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class BillFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        BillAdapter.BillAdapterListener {

    private List<Bill> billList = new ArrayList<>();
    private BaseApiServices api;
    private BillAdapter bAdapter;

    @BindView(R.id.recyclerview_bill)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_bill) SwipeRefreshLayout swipeRefreshLayout;


    public BillFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill, container, false);

        ButterKnife.bind(this,view);

        api = RetrofitClient.getClient().create(BaseApiServices.class);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getBillData();
            }
        });

        bAdapter = new BillAdapter(getActivity(),billList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(bAdapter);

        /*
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                String billString = new Gson().toJson(billList.get(position));
                Bundle bundle = new Bundle();
                bundle.putString("billString",billString);
                Fragment fragment = new DetailBillFragment();
                        fragment.setArguments(bundle);
                ((MainActivity) getActivity()).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame,fragment)
                        .addToBackStack(null).commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        */




        return view;
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        getBillData();
    }

    @Override
    public void onClick(View view,int position){

        String billString = new Gson().toJson(billList.get(position));
        Intent i = new Intent(getActivity().getBaseContext(), DetailBillActivity.class);
        i.putExtra("billString",billString);
        getActivity().startActivity(i);
    }



    private void getBillData(){
        swipeRefreshLayout.setRefreshing(true);
        String token = Session.getToken(getActivity());
        api.getBillList(token,"kurma","100%kurma").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBill>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseBill responseBill) {
                        if (responseBill.getStatus() == 1){
                            billList.clear();
                            for (Bill bill : responseBill.getListBill()){
                                bill.setColor(getRandomMaterialColor("500"));
                                billList.add(bill);
                            }
                            bAdapter.notifyDataSetChanged();

                        } else {
                            Toast.makeText(getActivity(),responseBill.getMsg(),Toast.LENGTH_SHORT).show();
                        }



                        //Toast.makeText(getActivity(),new Gson().toJson(responseKavling.getKavlingList()),Toast.LENGTH_SHORT).show();


                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        //bAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(),"Completed",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    private int getRandomMaterialColor(String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = getResources().getIdentifier("mdcolor_" + typeColor, "array", getActivity().getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }

}
