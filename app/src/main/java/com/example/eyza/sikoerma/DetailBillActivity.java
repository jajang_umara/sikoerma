package com.example.eyza.sikoerma;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.BaseResponse;
import com.example.eyza.sikoerma.model.Bill;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class DetailBillActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.table_container) TableLayout table;
    @BindView(R.id.confirm_payment) Button buttonConfirm;

    private Calendar myCalendar;
    private AwesomeValidation awesomeValidation;
    private ImageView preview;
    private Uri pictureUri;
    private BaseApiServices api;
    private CompositeDisposable disposable = new CompositeDisposable();
    private Button openGallery;
    private String mediaPath;
    private ProgressDialog pd;
    private int billId,method=1;
    private EditText amount,tanggal,description;
    private RadioButton transfer,direct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_bill);
        getSupportActionBar().hide();

        ButterKnife.bind(this);

        Intent i = getIntent();

        Bill bill = new Gson().fromJson(i.getStringExtra("billString"),Bill.class);
            billId = bill.getId();

            if (bill.getStatus() != 0){
                buttonConfirm.setVisibility(View.INVISIBLE);
            }

        myCalendar = Calendar.getInstance();

        try {
            JSONObject json = new JSONObject(i.getStringExtra("billString"));
            //Iterator<String> iter = json.keys();
            for (Iterator<String> iter = json.keys(); iter.hasNext();){
                String key = iter.next();
                if (Arrays.asList(new String[]{"color","id","order_id","billing_type_id","status","customer_id"}).contains(key)) {
                    continue;
                }
                String title = key.replace("_"," ");
                TableRow tr  = new TableRow(this);
                tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                TextView tv1 = new TextView(this);
                tv1.setText(title);
                tv1.setTextSize(16);
                tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                tr.addView(tv1);
                TextView tv3 = new TextView(this);
                tv3.setText(" : ");
                tr.addView(tv3);
                TextView tv2 = new TextView(this);
                tv2.setText(json.getString(key));
                tv2.setTextSize(16);
                tr.addView(tv2);
                table.addView(tr);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");

        api = RetrofitClient.getClient().create(BaseApiServices.class);
    }

    @OnClick(R.id.confirm_payment)
    void confirmPayment(){
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewZ = inflater.inflate(R.layout.form_confirm_payment,null);
        tanggal = (EditText) viewZ.findViewById(R.id.tanggal);
        tanggal.setFocusable(false);

        preview = (ImageView)viewZ.findViewById(R.id.preview);

        amount = (EditText)viewZ.findViewById(R.id.amount);
        description = (EditText)viewZ.findViewById(R.id.keterangan);
        transfer = (RadioButton)viewZ.findViewById(R.id.transfer);
        direct = (RadioButton)viewZ.findViewById(R.id.direct);




        tanggal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    DatePickerDialog dialog = new DatePickerDialog(DetailBillActivity.this, DetailBillActivity.this, myCalendar.get(Calendar.YEAR),
                            myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                    dialog.show();
                }
                return false;
            }
        });

        openGallery = (Button)viewZ.findViewById(R.id.take_gallery);
        openGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(DetailBillActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED ) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);
                } else {
                    ActivityCompat.requestPermissions(DetailBillActivity.this, new String[] {
                                    Manifest.permission.READ_EXTERNAL_STORAGE },
                            101);
                }
            }
        });
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("Form Konfirmasi");
        builder1.setView(viewZ);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "KIRIM",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(amount.getText().toString().isEmpty() || tanggal.getText().toString().isEmpty()){
                            Toast.makeText(DetailBillActivity.this, "Nominal atau tanggal masih kosong", Toast.LENGTH_SHORT).show();
                        } else {
                            sendConfirmPayment();
                        }

                    }
                });

        builder1.setNegativeButton(
                "BATAL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        tanggal.setText(sdformat.format(myCalendar.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            pictureUri = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(pictureUri, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            mediaPath = cursor.getString(columnIndex);

            Glide.with(this)
                    .load(mediaPath)
                    .apply(new RequestOptions().fitCenter().override(300))
                    .into(preview);

            cursor.close();
        }
    }

    void sendConfirmPayment(){

                pd.show();
                String token = Session.getToken(this);
                LinkedHashMap<String, RequestBody> data = new LinkedHashMap<String, RequestBody>();
                data.put("login_token", RequestBody.create(MediaType.parse("text/plain"), token));
                data.put("api_username", RequestBody.create(MediaType.parse("text/plain"), "kurma"));
                data.put("api_password", RequestBody.create(MediaType.parse("text/plain"), "100%kurma"));
                data.put("bill_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(billId)));
                data.put("amount", RequestBody.create(MediaType.parse("text/plain"), amount.getText().toString().trim()));
                data.put("pay_date", RequestBody.create(MediaType.parse("text/plain"), tanggal.getText().toString().trim()));
                data.put("description", RequestBody.create(MediaType.parse("text/plain"), description.getText().toString().trim()));
                if (transfer.isChecked()) {
                    method = 1;
                }

                if (direct.isChecked()) {
                    method = 2;
                }

                data.put("pay_method", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(method)));
                File file = new File(mediaPath);

                // Parsing any Media type file
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image_doc", file.getName(), requestBody);

                disposable.add(api.confirmPayment(fileToUpload, data)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<BaseResponse>() {
                            @Override
                            public void onSuccess(BaseResponse baseResponse) {
                                pd.dismiss();
                                Toast.makeText(DetailBillActivity.this, baseResponse.getMsg(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                pd.dismiss();
                                Toast.makeText(DetailBillActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                            }

                        }));



    }






}
