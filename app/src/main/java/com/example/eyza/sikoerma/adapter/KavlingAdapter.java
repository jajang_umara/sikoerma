package com.example.eyza.sikoerma.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.eyza.sikoerma.R;
import com.example.eyza.sikoerma.model.Kavling;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class KavlingAdapter extends RecyclerView.Adapter<KavlingAdapter.MyViewHolder> {

    public static List<Kavling> kavlingList = new ArrayList<>();
    private KavlingAdapterListener listener;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        @BindViews({R.id.no_kavling,R.id.nama_proyek,R.id.site,R.id.status,R.id.icon_text,R.id.kondisi})
        List<TextView> txtViews;
        @BindView(R.id.icon_profile) ImageView profile;
        @BindView(R.id.page_container) LinearLayout container;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);

        }


    }

    public KavlingAdapter(Context context,List<Kavling> kavlingList) {
        this.kavlingList = kavlingList;

        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kavling_list_row2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Kavling kavling = kavlingList.get(position);
        holder.txtViews.get(0).setText(" : "+String.valueOf(kavling.getKavlingNumber()));
        holder.txtViews.get(1).setText(" : "+kavling.getProjectName());
        holder.txtViews.get(2).setText(" : "+kavling.getProjectSite());
        holder.txtViews.get(3).setText(" : "+kavling.getStatusLegal());
        holder.txtViews.get(4).setText(kavling.getProjectName().substring(0,1));
        holder.txtViews.get(5).setText(" : "+kavling.getKondisiKavling());
        holder.profile.setImageResource(R.drawable.bg_circle);
        holder.profile.setColorFilter(kavling.getColor());

    }

    @Override
    public int getItemCount() {
        return kavlingList.size();
    }

    public interface KavlingAdapterListener {
        void onClickView(View view, int position);
    }
}
