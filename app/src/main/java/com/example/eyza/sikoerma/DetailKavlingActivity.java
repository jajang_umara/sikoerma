package com.example.eyza.sikoerma;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.KavlingBonus;
import com.example.eyza.sikoerma.model.KavlingDetail;
import com.example.eyza.sikoerma.model.KavlingProgress;
import com.example.eyza.sikoerma.model.ResponseDetailKavling;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class DetailKavlingActivity extends AppCompatActivity {

    private int kavlingId;
    private BaseApiServices api;
    private ProgressDialog pd;
    private CompositeDisposable disposable = new CompositeDisposable();

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.table_kavling) TableLayout table;
    @BindView(R.id.table_bonus) TableLayout table2;
    @BindView(R.id.table_progress) TableLayout table3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kavling);
        getSupportActionBar().hide();

        ButterKnife.bind(this);

        Intent i = getIntent();
        kavlingId = i.getIntExtra("kavling_id",0);

        api = RetrofitClient.getClient().create(BaseApiServices.class);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");

        mToolbar.setTitle("Kavling Detail");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.darkblue));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });




        getKavlingDetails();
    }

    public void getKavlingDetails() {
        pd.show();
        String token = Session.getToken(this);
        disposable.add(api.getKavlingDetails(token,kavlingId,"kurma","100%kurma")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResponseDetailKavling>(){
                            @Override
                            public void onSuccess(ResponseDetailKavling response) {
                                pd.dismiss();
                                if (response.getStatus() == 1){
                                    //Toast.makeText(DetailKavlingActivity.this,new Gson().toJson(response),Toast.LENGTH_SHORT).show();
                                    KavlingDetail kavlingDetail = response.getKavlingDetail();
                                    try {
                                        JSONObject json = new JSONObject(new Gson().toJson(kavlingDetail));
                                        for (Iterator<String> iter = json.keys(); iter.hasNext();) {
                                            String key = iter.next();
                                            if (Arrays.asList(new String[]{"included_bonus","progress"}).contains(key)) {
                                                continue;
                                            }

                                            String title = key.replace("_"," ");
                                            TableRow tr  = new TableRow(getApplicationContext());
                                            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                                                    TableRow.LayoutParams.WRAP_CONTENT));
                                            TextView tv1 = new TextView(getApplicationContext());
                                            tv1.setText(title);
                                            tv1.setTextSize(16);
                                            tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                                            tv1.setTextColor(Color.BLACK);
                                            tr.addView(tv1);
                                            TextView tv3 = new TextView(getApplicationContext());
                                            tv3.setText(" : ");
                                            tv3.setTextColor(Color.BLACK);
                                            tr.addView(tv3);
                                            TextView tv2 = new TextView(getApplicationContext());
                                            tv2.setText(json.getString(key));
                                            tv2.setTextSize(16);
                                            tv2.setTextColor(Color.BLACK);
                                            tr.addView(tv2);
                                            table.addView(tr);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    LayoutInflater inflater = LayoutInflater.from(DetailKavlingActivity.this); // or (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                                    if (kavlingDetail.getKavlingBonuses().size() > 0 ) {
                                        int i = 1;
                                        for (KavlingBonus kavlingBonus : kavlingDetail.getKavlingBonuses()) {
                                            View view = inflater.inflate(R.layout.table_layout, null);
                                            TextView tv1 = (TextView) view.findViewById(R.id.td1);
                                            tv1.setText(String.valueOf(i));
                                            TextView tv2 = (TextView) view.findViewById(R.id.td2);
                                            tv2.setText(kavlingBonus.getItem());
                                            TextView tv3 = (TextView) view.findViewById(R.id.td3);
                                            tv3.setText(String.valueOf(kavlingBonus.getQty()));
                                            table2.addView(view, i);
                                            i++;
                                        }
                                    }
                                    if (kavlingDetail.getProgress().size() > 0 ) {
                                         final ImagePopup  imagePopup = new ImagePopup(DetailKavlingActivity.this);
                                         int j = 1;
                                         for (final KavlingProgress kavlingProgress : kavlingDetail.getProgress()) {
                                             View view = inflater.inflate(R.layout.table_layout2, null);
                                             TextView tv1 = (TextView) view.findViewById(R.id.td1);
                                             tv1.setText(kavlingProgress.getDate());
                                             TextView tv2 = (TextView) view.findViewById(R.id.td2);
                                             tv2.setText(kavlingProgress.getDescription());
                                             if (kavlingProgress.getImage1() != null) {
                                                 ImageView iv1 = (ImageView) view.findViewById(R.id.td3);
                                                 Glide.with(getApplicationContext())
                                                         .load(kavlingProgress.getImage1())
                                                         .apply(new RequestOptions().override(120))
                                                         .into(iv1);
                                                 imagePopup.setBackgroundColor(Color.TRANSPARENT);


                                                 iv1.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         imagePopup.initiatePopupWithGlide(kavlingProgress.getImage1());
                                                         imagePopup.viewPopup();
                                                     }
                                                 });
                                             }

                                             if (kavlingProgress.getImage2() != null) {
                                                 ImageView iv2 = (ImageView) view.findViewById(R.id.td4);
                                                 Glide.with(getApplicationContext())
                                                         .load(kavlingProgress.getImage2())
                                                         .apply(new RequestOptions().override(120))
                                                         .into(iv2);

                                                 iv2.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         imagePopup.initiatePopupWithGlide(kavlingProgress.getImage2());
                                                         imagePopup.viewPopup();
                                                     }
                                                 });
                                             }

                                             if (kavlingProgress.getImage3() != null) {
                                                 ImageView iv3 = (ImageView) view.findViewById(R.id.td5);
                                                 Glide.with(getApplicationContext())
                                                         .load(kavlingProgress.getImage3())
                                                         .apply(new RequestOptions().override(120))
                                                         .into(iv3);

                                                 iv3.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         imagePopup.initiatePopupWithGlide(kavlingProgress.getImage3());
                                                         imagePopup.viewPopup();
                                                     }
                                                 });
                                             }
                                             table3.addView(view, j);
                                             j++;
                                         }
                                    }



                                } else {
                                    Toast.makeText(DetailKavlingActivity.this,response.getMsg(),Toast.LENGTH_SHORT).show();
                                }


                            }
                            @Override
                            public void onError(Throwable e) {
                                pd.dismiss();
                                Toast.makeText(DetailKavlingActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();

                            }
                        }));
    }


}
