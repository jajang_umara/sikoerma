package com.example.eyza.sikoerma.apiservices;

import com.example.eyza.sikoerma.model.BaseResponse;
import com.example.eyza.sikoerma.model.Kavling;
import com.example.eyza.sikoerma.model.ResponseBill;
import com.example.eyza.sikoerma.model.ResponseDetailKavling;
import com.example.eyza.sikoerma.model.ResponseKavling;
import com.example.eyza.sikoerma.model.ResponseLogin;
import com.example.eyza.sikoerma.model.ResponseLogout;
import com.example.eyza.sikoerma.model.User;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface BaseApiServices {

    @FormUrlEncoded
    @POST("api/getKavlingList")
    Observable<ResponseKavling> getMyKavling(@Field("login_token") String token,
                                             @Field("api_username") String apiUser,
                                             @Field("api_password") String apiPass);

    @FormUrlEncoded
    @POST("api/getBillList")
    Observable<ResponseBill> getBillList(@Field("login_token") String token,
                                          @Field("api_username") String apiUser,
                                          @Field("api_password") String apiPass);

    @FormUrlEncoded
    @POST("api/login")
    Single<ResponseLogin> getLogin(@Field("username") String username,
                                   @Field("password") String password,
                                   @Field("api_username") String apiUser,
                                   @Field("api_password") String apiPass);

    @FormUrlEncoded
    @POST("api/logout")
    Single<ResponseLogout> getLogout(@Field("login_token") String token,
                                     @Field("api_username") String apiUser,
                                     @Field("api_password") String apiPass);

    @FormUrlEncoded
    @POST("api/getKavlingDetails")
    Single<ResponseDetailKavling> getKavlingDetails(@Field("login_token") String token,
                                                    @Field("kavling_id") int kavling_id,
                                                    @Field("api_username") String apiUser,
                                                    @Field("api_password") String apiPass);

    @Multipart
    @POST("api/confirmPayment")
    Single<BaseResponse> confirmPayment(@Part MultipartBody.Part file, @PartMap Map<String,RequestBody> queryMap);

    @FormUrlEncoded
    @POST("api/changePassword")
    Single<BaseResponse> changePassword(@FieldMap Map<String,String> fields);
}
