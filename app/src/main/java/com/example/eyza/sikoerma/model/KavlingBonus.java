package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

public class KavlingBonus {
    @SerializedName("item_name") String item;
    private int qty;

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
