package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {
    private int status;
    private int code;
    @SerializedName("msg") private String message;
    @SerializedName("data") private User user;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
