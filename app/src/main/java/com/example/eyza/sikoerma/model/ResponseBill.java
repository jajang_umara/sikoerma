package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseBill {
    private int status;
    private String msg;
    @SerializedName("data") private List<Bill> listBill;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Bill> getListBill() {
        return listBill;
    }

    public void setListBill(List<Bill> listBill) {
        this.listBill = listBill;
    }
}
