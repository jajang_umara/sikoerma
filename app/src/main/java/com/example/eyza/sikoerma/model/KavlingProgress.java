package com.example.eyza.sikoerma.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class KavlingProgress {
    @SerializedName("progress_date") private String date;
    private String description,image1,image2,image3;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
