package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseKavling {

    private int status;
    private String msg;
    @SerializedName("data") private List<Kavling> kavlingList;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Kavling> getKavlingList() {
        return kavlingList;
    }

    public void setKavlingList(List<Kavling> kavlingList) {
        this.kavlingList = kavlingList;
    }
}
