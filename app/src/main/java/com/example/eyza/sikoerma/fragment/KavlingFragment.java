package com.example.eyza.sikoerma.fragment;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eyza.sikoerma.DetailBillActivity;
import com.example.eyza.sikoerma.DetailKavlingActivity;
import com.example.eyza.sikoerma.R;
import com.example.eyza.sikoerma.adapter.KavlingAdapter;
import com.example.eyza.sikoerma.adapter.RecyclerTouchListener;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.Kavling;
import com.example.eyza.sikoerma.model.ResponseKavling;
import com.example.eyza.sikoerma.model.ResponseLogin;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class KavlingFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recyclerview_kavling) RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_kavling) SwipeRefreshLayout swipeRefreshLayout;

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    private BaseApiServices api;
    private List<Kavling> listKavling = new ArrayList<>();
    private KavlingAdapter kAdapter;

    public KavlingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kavling, container, false);

        ButterKnife.bind(this,view);

        api = RetrofitClient.getClient().create(BaseApiServices.class);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getKavlingData();
            }
        });

        kAdapter = new KavlingAdapter(getActivity(),listKavling);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(kAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Kavling kavling = listKavling.get(position);
                Intent i = new Intent(getActivity().getBaseContext(), DetailKavlingActivity.class);
                i.putExtra("kavling_id",kavling.getId());
                getActivity().startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.app_bar_menu_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);

                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        getKavlingData();
    }

    private void getKavlingData(){
        swipeRefreshLayout.setRefreshing(true);
        String token = Session.getToken(getActivity());
        api.getMyKavling(token,"kurma","100%kurma").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseKavling>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseKavling responseKavling) {
                       if (responseKavling.getStatus() == 1){
                           listKavling.clear();
                           for (Kavling kavling : responseKavling.getKavlingList()){
                               kavling.setColor(getRandomMaterialColor("500"));
                               listKavling.add(kavling);
                           }
                           kAdapter.notifyDataSetChanged();

                       } else {
                           Toast.makeText(getActivity(),responseKavling.getMsg(),Toast.LENGTH_SHORT).show();
                       }



                        //Toast.makeText(getActivity(),new Gson().toJson(responseKavling.getKavlingList()),Toast.LENGTH_SHORT).show();


                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        kAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(),"Completed",Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    private int getRandomMaterialColor(String typeColor) {
        int returnColor = Color.GRAY;
        int arrayId = getResources().getIdentifier("mdcolor_" + typeColor, "array", getActivity().getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }



}
