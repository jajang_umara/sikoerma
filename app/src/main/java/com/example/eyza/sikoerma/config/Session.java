package com.example.eyza.sikoerma.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    static final String LOGIN_STATUS = "login_status";
    static final String USER_ID = "user_id";
    static final String USER_DATA = "user_data";
    static final String TOKEN = "token";


    public static SharedPreferences getSharedPreference(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public static boolean getLoginStatus(Context context){
        return getSharedPreference(context).getBoolean(LOGIN_STATUS,false);
    }

    public static void setLoginStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.apply();
    }

    public static int getUserId(Context context){
        return getSharedPreference(context).getInt(USER_ID,0);
    }

    public static void setUserId(Context context, int id){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }

    public static String getUserData(Context context) {
        return getSharedPreference(context).getString(USER_DATA,"{}");
    }

    public static void setUserData(Context context, String user){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(USER_DATA, user);
        editor.apply();
    }

    public static String getToken(Context context) {
        return getSharedPreference(context).getString(TOKEN,"");
    }

    public static void setToken(Context context, String token){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }


}
