package com.example.eyza.sikoerma.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eyza.sikoerma.MainActivity;
import com.example.eyza.sikoerma.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailBillFragment extends Fragment {

    private TableLayout table;
    public DetailBillFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.fragment_detail_bill, container, false);

        ButterKnife.bind(this,view);

        table = (TableLayout) view.findViewById(R.id.table_container);

        Bundle bundle = this.getArguments();




        try {
            JSONObject json = new JSONObject(bundle.getString("billString"));
            //Iterator<String> iter = json.keys();
            for (Iterator<String> iter = json.keys();iter.hasNext();){
                String key = iter.next();
                TableRow tr  = new TableRow(getActivity());
                tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                TextView tv1 = new TextView(getActivity());
                tv1.setText(key);
                tr.addView(tv1);
                TextView tv2 = new TextView(getActivity());
                tv2.setText(json.getString(key));
                tr.addView(tv2);
                table.addView(tr);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }





        return view;
    }



}
