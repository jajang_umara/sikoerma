package com.example.eyza.sikoerma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.User;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    @BindViews({R.id.nama_head,R.id.profile_email,R.id.profile_phone,R.id.profile_id,R.id.profile_gender,R.id.profile_occupation,
    R.id.profile_town,R.id.profile_address,R.id.profile_tax})
    List<TextView> txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Profile");
        ButterKnife.bind(this);

        Gson gson = new Gson();

        String stringUser = Session.getUserData(this);
        User user = gson.fromJson(stringUser,User.class);

        txtView.get(0).setText(user.getName());
        txtView.get(1).setText(user.getEmail());
        txtView.get(2).setText(user.getPhoneNumber());
        txtView.get(3).setText(user.getCustomerNo());
        txtView.get(4).setText(user.getGender());
        txtView.get(5).setText(user.getOccupation());
        txtView.get(6).setText(user.getCitizenship());
        txtView.get(7).setText(user.getAddress());
        txtView.get(8).setText(user.getTaxNumber());
    }

    @OnClick(R.id.btn_back)
    public void backButton(){
        startActivity(new Intent(getApplicationContext(),ForgotPasswordActivity.class));
        //finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
