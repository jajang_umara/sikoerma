package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

public class Kavling {

    @SerializedName("kavling_condition") private String kondisiKavling;
    @SerializedName("status_legal_product") private String statusLegal;
    private int id;
    @SerializedName("kavling_no") private int kavlingNumber;
    @SerializedName("project_name") private String projectName;
    @SerializedName("site_name") private String projectSite;
    private String blok;
    private int color = -1;

    public String getKondisiKavling() {
        return kondisiKavling;
    }

    public void setKondisiKavling(String kondisiKavling) {
        this.kondisiKavling = kondisiKavling;
    }

    public String getStatusLegal() { return statusLegal; }

    public void setStatusLegal(String statusLegal) { this.statusLegal = statusLegal; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKavlingNumber() {
        return kavlingNumber;
    }

    public void setKavlingNumber(int kavlingNumber) {
        this.kavlingNumber = kavlingNumber;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectSite() {
        return projectSite;
    }

    public void setProjectSite(String projectSite) {
        this.projectSite = projectSite;
    }

    public String getBlok() {
        return blok;
    }

    public void setBlok(String blok) {
        this.blok = blok;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
