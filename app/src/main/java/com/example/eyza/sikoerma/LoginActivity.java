package com.example.eyza.sikoerma;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.model.ResponseLogin;
import com.example.eyza.sikoerma.model.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    private AwesomeValidation awesomeValidation;
    private ProgressDialog pd;
    private BaseApiServices api;
    private CompositeDisposable disposable = new CompositeDisposable();

    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        AwesomeValidation.disableAutoFocusOnFirstFailure();

        awesomeValidation.addValidation(this,R.id.username, RegexTemplate.NOT_EMPTY,R.string.required_field);
        awesomeValidation.addValidation(this,R.id.password, RegexTemplate.NOT_EMPTY,R.string.required_field);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");

        api = RetrofitClient.getClient().create(BaseApiServices.class);
    }

    @OnClick(R.id.btn_login)
    public void login() {


        if (awesomeValidation.validate()){
            pd.show();
            disposable.add(api.getLogin(username.getText().toString(),password.getText().toString(),"kurma","100%kurma")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<ResponseLogin>(){
                        @Override
                         public void onSuccess(ResponseLogin responseLogin) {
                            if (responseLogin.getStatus() == 1){
                                Gson gson = new Gson();
                                User user = responseLogin.getUser();
                                Session.setLoginStatus(getApplicationContext(),true);
                                Session.setUserData(getApplicationContext(),gson.toJson(user));
                                Session.setToken(getApplicationContext(),user.getToken());
                                Toast.makeText(getApplicationContext(),responseLogin.getMessage(),Toast.LENGTH_SHORT).show();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(LoginActivity.this,MainActivity.class);
                                        startActivity(i);
                                        finish();
                                    }
                                },1000);
                            } else {
                                Toast.makeText(getApplicationContext(),responseLogin.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                            pd.dismiss();
                        }

                        @Override
                         public void onError(Throwable e) {
                            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    }));
        }
    }




}
