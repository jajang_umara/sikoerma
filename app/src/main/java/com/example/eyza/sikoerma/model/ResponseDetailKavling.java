package com.example.eyza.sikoerma.model;

import com.google.gson.annotations.SerializedName;

public class ResponseDetailKavling extends BaseResponse {
    @SerializedName("data") private KavlingDetail kavlingDetail;

    public KavlingDetail getKavlingDetail() {
        return kavlingDetail;
    }

    public void setKavlingDetail(KavlingDetail kavlingDetail) {
        this.kavlingDetail = kavlingDetail;
    }
}
