package com.example.eyza.sikoerma.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.eyza.sikoerma.R;
import com.example.eyza.sikoerma.model.Bill;


import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.MyViewHolder> {
    public static List<Bill> billList = new ArrayList<>();
    private BillAdapterListener listener;
    private Context context;
    private LayoutInflater inflater;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.billing_no,R.id.billing_date,R.id.billing_status,R.id.icon_text,R.id.amount})
        List<TextView> txtViews;
        @BindView(R.id.icon_profile)
        ImageView profile;
        @BindView(R.id.page_container)
        RelativeLayout container;



        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(v,getAdapterPosition());
                    v.performHapticFeedback(HapticFeedbackConstants.CONTEXT_CLICK);
                }
            });

        }


    }

    public BillAdapter(Context context, List<Bill> billList,BillAdapterListener listener) {
        this.billList = billList;
        this.listener = listener;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BillAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater
                .inflate(R.layout.bill_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BillAdapter.MyViewHolder holder, int position) {
        Bill bill = billList.get(position);
        holder.txtViews.get(0).setText(bill.getBillingNumber());
        holder.txtViews.get(1).setText(bill.getBillingDate());
        holder.txtViews.get(2).setText(bill.getStatusName());

        holder.txtViews.get(3).setText("Rp");
        holder.txtViews.get(4).setText("Rp. "+NumberFormat.getNumberInstance(Locale.US).format(bill.getAmount()));
        holder.profile.setImageResource(R.drawable.bg_circle);
        holder.profile.setColorFilter(bill.getColor());

    }

    @Override
    public int getItemCount() {
        return billList.size();
    }

    public interface BillAdapterListener {
        public void onClick(View view,int Position);
    }


}
