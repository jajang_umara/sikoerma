package com.example.eyza.sikoerma;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;

import android.support.v4.app.Fragment;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eyza.sikoerma.apiservices.BaseApiServices;
import com.example.eyza.sikoerma.apiservices.RetrofitClient;
import com.example.eyza.sikoerma.config.Helper;
import com.example.eyza.sikoerma.config.Session;
import com.example.eyza.sikoerma.fragment.BillFragment;
import com.example.eyza.sikoerma.fragment.DetailBillFragment;
import com.example.eyza.sikoerma.fragment.HomeFragment;
import com.example.eyza.sikoerma.fragment.KavlingFragment;
import com.example.eyza.sikoerma.model.ResponseLogin;
import com.example.eyza.sikoerma.model.ResponseLogout;
import com.example.eyza.sikoerma.model.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBar actionBar;
    private BaseApiServices api;
    private CompositeDisposable disposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        Gson gson = new Gson();

        String userString = Session.getUserData(MainActivity.this);
        User user = gson.fromJson(userString,User.class);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = (TextView) headerView.findViewById(R.id.textView);
        navUsername.setText("Selamat Datang : "+user.getName());

        // Get ActionBar
        actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.

        actionBar.setTitle("SIKOERMA HOME");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        api = RetrofitClient.getClient().create(BaseApiServices.class);


        loadFragment(new HomeFragment());


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_kavling) {
            actionBar.setTitle("KAVLING SAYA");
            loadFragment(new KavlingFragment());

        } else if (id == R.id.nav_bill) {
            actionBar.setTitle("TAGIHAN");
            loadFragment(new BillFragment());

        } else if (id == R.id.nav_profile) {
            Intent i = new Intent(MainActivity.this,ProfileActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_exit) {

            AlertDialog.Builder alert = Helper.createDialog(MainActivity.this,"Are You Sure?","Do You Want Exit From this App");

            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String token = Session.getToken(MainActivity.this);
                   disposable.add(api.getLogout(token,"kurma","100%kurma")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<ResponseLogout>(){
                        @Override
                        public void onSuccess(ResponseLogout responseLogout) {

                            PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().clear().commit();
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);

                        }
                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();

                        }
                    }));

                    //PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().clear().commit();
                    //android.os.Process.killProcess(android.os.Process.myPid());
                    //System.exit(1);
                }
            });



            AlertDialog alertDialog = alert.create();
            alertDialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,fragment)
                .addToBackStack(null).commit();
    }
}
