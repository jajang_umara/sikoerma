package com.example.eyza.sikoerma;


import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eyza.sikoerma.config.Session;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.logo) ImageView logo;
    @BindView(R.id.text1) TextView text1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Animation myanim = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        text1.setVisibility(View.INVISIBLE);
        logo.startAnimation(myanim);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                text1.setVisibility(View.VISIBLE);
                Animation textAnim = AnimationUtils.loadAnimation(SplashActivity.this,R.anim.zoom_out);
                text1.startAnimation(textAnim);
            }

        }, 2000);

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i;
                if (Session.getLoginStatus(SplashActivity.this)){
                    i = new Intent(SplashActivity.this,MainActivity.class);
                } else {
                    i = new Intent(SplashActivity.this,LoginActivity.class);
                }

                startActivity(i);
                finish();
            }

        }, 4000);
    }
}
