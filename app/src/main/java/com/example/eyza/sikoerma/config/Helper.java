package com.example.eyza.sikoerma.config;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.content.Context;

public class Helper {

    public static AlertDialog.Builder createDialog(Context context,String title,String Message) {
        final AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
        alertDialog1.setTitle(title);
        alertDialog1.setMessage(Message);
        alertDialog1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        return alertDialog1;
    }

}
